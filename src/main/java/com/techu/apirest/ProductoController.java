package com.techu.apirest;


import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductoController {

    static final String BASEURL = "/apitechu/v1";
    private static ArrayList<ProductoModel> productos = new ArrayList<ProductoModel>();
    static {
        productos.add(new ProductoModel(1,"descripcion 1",130.1));
        productos.add(new ProductoModel(2,"descripcion 2",131.1));
        productos.add(new ProductoModel(3,"descripcion 3",132.1));
        productos.add(new ProductoModel(4,"descripcion 4",133.1));
    }

    @GetMapping(BASEURL)
    public String index(){
        return "Página base no construida";
    }


    @GetMapping(BASEURL + "/productos")
    public ArrayList<ProductoModel> getProductos(){
        return productos;
    }

    // /productos/1   meter variables

    @GetMapping(BASEURL + "/productos/{id}")
    public ProductoModel getProductoId(@PathVariable int id){
        for(int i=0; i<productos.size() ; i++){
            if (productos.get(i).getId()== id){
                return productos.get(i);
            }
        }
        ProductoModel noProducto =new ProductoModel(0,"null",0.0);
        return noProducto;
    }

    //añadir producto nuevo
    @PostMapping(BASEURL + "/productos")
    public ProductoModel postProductos(@RequestBody ProductoModel newProduct){
        productos.add(newProduct);
        return newProduct;

    }

    //actualizar producto existente
    @PutMapping(BASEURL + "/productos/{id}")
    public ProductoModel putProductos(@RequestBody ProductoModel producto, @PathVariable int id){

        for(int i=0; i<productos.size() ; i++){
            if (productos.get(i).getId()== id){
                productos.set(i,producto);
                return productos.get(i);
            }
        }
        ProductoModel noProducto =new ProductoModel(0,"null",0.0);
        return noProducto;
    }

    // ahora un delete
    @DeleteMapping (BASEURL + "/productos/{id}")
    public String delProductos(@PathVariable int id){
        for(int i=0; i<productos.size() ; i++){
            if (productos.get(i).getId()== id){
                productos.remove(i);
                return "Producto "+id+" borrado correctamente";
            }
        }
        return "Producto no encontrado, no se elimina";

    }

    @PatchMapping (BASEURL + "/productos/{id}")
    public ProductoModel patchProductos (@RequestBody ProductoPrecio productoPrecio, @PathVariable int id){

        return productoPrecio.savePrecio(productoPrecio,id);
        }
}

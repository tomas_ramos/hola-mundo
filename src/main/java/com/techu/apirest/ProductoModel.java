package com.techu.apirest;


public class ProductoModel {

    private Integer id;
    private String descripcion;
    private Double precio;

    //code -- generate -- constructor


    public ProductoModel(Integer id, String descripcion, Double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    //creamos tb un constructor vacío por lo que pueda pasar...
    public ProductoModel(){}


    //ahora añado getters and setters


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}

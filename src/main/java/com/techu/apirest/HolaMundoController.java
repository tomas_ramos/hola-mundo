package com.techu.apirest;

import org.springframework.web.bind.annotation.*;

@RestController  //indica que esta clase va a tener la funcionalidad de un contenedor controlador, que va a estar ejecutándose y escuchando
public class HolaMundoController {

    public static final String URLBASE = "/api/v1";

    //@RequestMapping( value="/api/v1/saludos", method = RequestMethod.GET) //define un método get directamente. cuando desde un cliente se mapee con un "/" dará un get... lo mira en la raiz, así que http://localhost:8080  accede a esta rutina
                                       //con esto quedaría la dirección http://localhost:8080/api/v1/saludos

    @GetMapping(URLBASE + "/saludos")  //ahora saludos admite como parámetro nombre  http://localhost:8080/api/v1/saludos?nombre=Tomas
    public String saludar(@RequestParam(value="nombre", defaultValue = "valor-defecto") String name, @RequestParam String color){
        return "Hola techU, soy " +name+" y me gusta el color "+color;   //http://localhost:8080/api/v1/saludos?nombre=Tomas&color=verde
    }

}

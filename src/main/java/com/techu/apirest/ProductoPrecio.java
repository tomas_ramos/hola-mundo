package com.techu.apirest;

import java.util.ArrayList;

public class ProductoPrecio {
    private Integer id;
    private Double precio;

    private static ArrayList<ProductoModel> productos = new ArrayList<ProductoModel>();
    static {
        productos.add(new ProductoModel(1,"descripcion 1",130.1));
        productos.add(new ProductoModel(2,"descripcion 2",131.1));
        productos.add(new ProductoModel(3,"descripcion 3",132.1));
        productos.add(new ProductoModel(4,"descripcion 4",133.1));
    }

    public ProductoPrecio(Integer id, Double precio) {
        this.id = id;
        this.precio = precio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public ProductoModel savePrecio(ProductoPrecio productoPrecio, int id){
        ProductoModel productoTotal = new ProductoModel();
        for(int i=0; i<productos.size() ; i++){
            if (productos.get(i).getId()== id){
                productos.get(i).setPrecio(productoPrecio.getPrecio());
                return productos.get(i);
            }
        }
        ProductoModel noProducto =new ProductoModel(0,"null",0.0);

        return noProducto;
    }
}
